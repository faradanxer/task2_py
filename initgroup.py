import pandas as pd
import psycopg2
from sqlalchemy import create_engine
engine = create_engine('postgresql://test:123@localhost:5432/test')
file = 'littlegrouplist.xlsx'

xl = pd.ExcelFile(file)

print(xl.sheet_names)

df1 = xl.parse('Лист1')

df1.to_sql('students', engine)

conn = psycopg2.connect(dbname='test', user='test',
                        password='123', host='localhost')
cursor = conn.cursor()
cursor.execute('\dt')
cursor.fetchall()
