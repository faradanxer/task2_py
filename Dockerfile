FROM python:latest

WORKDIR ./app
RUN pip install virtualenv
RUN virtualenv venv
RUN source venv/bin/activate
RUN pip install openpyxl
RUN pip install setuptools
RUN pip install pandas
RUN pip install psycopg2
RUN pip install sqlalchemy

COPY initgroup.py ./
COPY littlegrouplist.xlsx ./

CMD [ "python","initgroup.py"]
